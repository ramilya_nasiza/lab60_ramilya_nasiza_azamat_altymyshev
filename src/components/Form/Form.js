import React from 'react';
import Input from "./Input/Input";
import './Form.css';
import Button from "./Button/Button";

const Form = props => {
  return (
      <div className="Form">
        <Input
            value={props.authorText}
            className="InputAuthor"
            onChange={props.authorChange}
            placeholder={'Author'}
        />
        <Input
            value={props.messageText}
            className="InputMessage"
            onChange={props.messageChange}
            placeholder={'Message'}
        />
        <Button
          onClick={props.sendMessage}
        />
      </div>
  );
};

export default Form;