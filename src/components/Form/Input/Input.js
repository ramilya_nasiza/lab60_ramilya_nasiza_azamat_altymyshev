import React from 'react';
import './Input.css';

const Input = props => {
  return (
      <input
          value={props.value}
          type="text"
          className={props.className}
          onChange={props.onChange}
          placeholder={props.placeholder}
      />
  );
};

export default Input;