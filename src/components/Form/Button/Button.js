import React from 'react';
import './Button.css';

const Button = props => {
  return (
      <button onClick={props.onClick} className="Button">Send</button>
  );
};

export default Button;