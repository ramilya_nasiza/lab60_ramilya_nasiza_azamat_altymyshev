import React, { Component } from 'react';
import Messages from "./components/Messages/Messages";
import Form from "./components/Form/Form";

import './App.css';

const url = 'http://146.185.154.90:8000/messages';

class App extends Component {

  state = {
    messages: [],
    authorText: '',
    messageText: '',
    lastDate: ''
  };

  getRequest = (url) => {
    fetch(url)
        .then((response) => {
          if (response.ok) {
            return response.json();
          }

          throw new Error('Request is not correct');
        })
        .then((messages) => {
          console.log(messages);
          if (messages.length !== 0 && messages.length > 15) {
            const lastDate = messages[messages.length - 1].datetime;
            this.setState({messages, lastDate});
          } else if (messages.length !== 0) {
            const thisMessages = [...this.state.messages];
            const lastDate = messages[messages.length - 1].datetime;
            for (var i = 0; i < messages.length; i++) {
              thisMessages.push(messages[i]);
            }
            this.setState({messages: thisMessages, lastDate});
          }
        })
  };

  changeHandlerAuthor = (event) => {
    const authorText = event.target.value;
    this.setState({authorText});
  };

  changeHandlerMessage = (event) => {
    const messageText = event.target.value;
    this.setState({messageText});
  };

  sendMessage = (message, author) => {
    if (message !== '' && author !== '') {
      clearInterval(this.interval);
      const data = new URLSearchParams();
      data.set('message', message);
      data.set('author', author);

      fetch(url, {
        method: 'post',
        body: data,
      });

      this.setState({authorText: '', messageText: ''});
      this.getLastMessages();
    } else {
      alert('You did not fill the form!');
    }
  };

  interval = () => {
    this.getRequest(url + "?datetime=" + this.state.lastDate);
  };

  getLastMessages = () => {
    clearInterval(this.interval);
    setInterval(this.interval, 3000)
  };

  componentDidMount() {
    this.getRequest(url);
    this.getLastMessages();
  }

  componentWillUnmount(){
    clearInterval(this.interval);
  }

  render() {
    return (
      <div className="App">
        <Messages
          messages={this.state.messages}
        />
        <Form
            authorText={this.state.authorText}
            authorChange={(event) => this.changeHandlerAuthor(event)}
            messageText={this.state.messageText}
            messageChange={(event) => this.changeHandlerMessage(event)}
            sendMessage={() => this.sendMessage(this.state.messageText, this.state.authorText)}
        />
      </div>
    );
  }
}

export default App;
