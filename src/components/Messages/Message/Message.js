import React from 'react';
import './Message.css';

const Message = props => {
  return (
      <div className="Message">
        <p>Time: <span>{props.time}</span></p>
        <p>Author: <span>{props.author}</span></p>
        <p>Message: <span>{props.message}</span></p>
      </div>
  );
};

export default Message;